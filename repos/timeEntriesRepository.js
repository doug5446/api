const db = require('../db');

module.exports = {
    async findByIDs(ids) {
        return await db
                    .from('time_entries')
                    .whereIn('work_order_id', ids)
    },
    async getAll() {
        return await db
                    .from('time_entries')
                    .limit(3)
    }
}
