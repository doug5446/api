const db = require('../db');

module.exports = {
    async findByID(id) {
        return await db
                    .from('work_orders')
                    .leftOuterJoin('users', {'work_orders.assigned_to_id': 'users.id'})
                    .select(workorderColumns)
                    .where('work_orders.id', id)
                    .where('work_orders.company_id', 1);
    }
}

let workorderColumns = ['work_orders.id', 
'title', 
'customer_id', 
'service_location_id', 
'contact_id', 
'status_id', 
'work_order_type_id', 
'description', 
'task', 
'estimated_reg_hours', 
'estimated_ot_hours', 
'estimated_mileage', 
'estimated_materials_cost', 
'work_order_priority_id', 
'work_orders.created', 
'created_by_id', 
'modified', 
'po_number', 
'customer_po_number', 
'assigned_to_id', 
'scheduled_date', 
'deleted', 
'quick_books_invoice_number', 
'is_billable', 
'maintenance_agreement_id', 
'maintenance_agreement_work_order_schedule_id', 
'approved', 
'schedule_id', 
'work_orders.company_id', 
'department_id', 
'has_been_reviewed', 
'work_order_number',
'review',
'users.full_name']