const db = require('../db');

module.exports = {
    async findByID(id) {
        return await db
                    .from('work_order_notes')
                    .where('work_order_id', id)
                    .where('note', 'like', '%not complete%')
                    .orderBy('created', 'desc')
    },
    async getAll() {
        return await db
                    .from('work_order_notes')
                    .where('note', 'like', '%not complete%')
                    
    }
}