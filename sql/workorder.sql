SELECT `WorkOrder`.`id`, 
       `WorkOrder`.`title`, 
       `WorkOrder`.`customer_id`, 
       `WorkOrder`.`service_location_id`, 
       `WorkOrder`.`contact_id`, 
       `WorkOrder`.`status_id`, 
       `WorkOrder`.`work_order_type_id`, 
       `WorkOrder`.`description`, 
       `WorkOrder`.`task`, 
       `WorkOrder`.`estimated_reg_hours`, 
       `WorkOrder`.`estimated_ot_hours`, 
       `WorkOrder`.`estimated_mileage`, 
       `WorkOrder`.`estimated_materials_cost`, 
       `WorkOrder`.`work_order_priority_id`, 
       `WorkOrder`.`created`, 
       `WorkOrder`.`created_by_id`, 
       `WorkOrder`.`modified`, 
       `WorkOrder`.`po_number`, 
       `WorkOrder`.`customer_po_number`, 
       `WorkOrder`.`assigned_to_id`, 
       `WorkOrder`.`scheduled_date`, 
       `WorkOrder`.`deleted`, 
       `WorkOrder`.`quick_books_invoice_number`, 
       `WorkOrder`.`is_billable`, 
       `WorkOrder`.`maintenance_agreement_id`, 
       `WorkOrder`.`maintenance_agreement_work_order_schedule_id`, 
       `WorkOrder`.`approved`, 
       `WorkOrder`.`schedule_id`, 
       `WorkOrder`.`company_id`, 
       `WorkOrder`.`department_id`, 
       `WorkOrder`.`has_been_reviewed`, 
       `WorkOrder`.`work_order_number`,
       `WorkOrder`.`review`, 
       ( 
       Concat("#", `WorkOrder`.`id`, " - ", 
       Substring(`WorkOrder`.`description`, 1, 30), " - assigned to ", (SELECT 
       `users`.`full_name` 
       FROM   `protech`.`users` 
       WHERE  `users`.`id` = `WorkOrder`.`assigned_to_id`)) ) AS 
       `WorkOrder__full_title`,
       ( CASE 
           WHEN `WorkOrder`.`maintenance_agreement_id` != 0 
                 OR `WorkOrder`.`maintenance_agreement_work_order_schedule_id` 
                    != 0 
         THEN 
           Concat("m", `WorkOrder`.`work_order_number`) 
           WHEN `WorkOrder`.`work_order_type_id` = 1 
                 OR `WorkOrder`.`work_order_type_id` = 2 THEN Concat("p", 
       `WorkOrder`.`work_order_number`) 
       ELSE Concat("w", `WorkOrder`.`work_order_number`) 
       end )                                                  AS 
       `WorkOrder__work_order_number_formatted`, 
       ( CASE 
           WHEN `WorkOrder`.`maintenance_agreement_id` = 0 
                AND `WorkOrder`.`maintenance_agreement_work_order_schedule_id` = 
                    0 THEN 
           0 
           ELSE 1 
         end )                                                AS 
       `WorkOrder__has_maintenance_agreement`, 
       ( CASE 
           WHEN ( `WorkOrder`.`maintenance_agreement_id` != 0 
                   OR `WorkOrder`.`maintenance_agreement_work_order_schedule_id` 
                      != 0 ) 
                AND `WorkOrder`.`approved` = 0 
                AND `WorkOrder`.`status_id` >= 9 THEN 1 
           ELSE 0 
         end )                                                AS 
       `WorkOrder__has_maintenance_agreement_not_approved`, 
       ( Date_format(`WorkOrder`.`created`, "%b %y") )        AS 
       `WorkOrder__work_order_date_formatted` 
FROM   `protech`.`work_orders` AS `WorkOrder` 
WHERE  `id` = 1867 
       AND `WorkOrder`.`company_id` = 1 
LIMIT  1 


`WorkOrder`.`id`, 
`WorkOrder`.`title`, 
`WorkOrder`.`customer_id`, 
`WorkOrder`.`service_location_id`, 
`WorkOrder`.`contact_id`, 
`WorkOrder`.`status_id`, 
`WorkOrder`.`work_order_type_id`, 
`WorkOrder`.`description`, 
`WorkOrder`.`task`, 
`WorkOrder`.`estimated_reg_hours`, 
`WorkOrder`.`estimated_ot_hours`, 
`WorkOrder`.`estimated_mileage`, 
`WorkOrder`.`estimated_materials_cost`, 
`WorkOrder`.`work_order_priority_id`, 
`WorkOrder`.`created`, 
`WorkOrder`.`created_by_id`, 
`WorkOrder`.`modified`, 
`WorkOrder`.`po_number`, 
`WorkOrder`.`customer_po_number`, 
`WorkOrder`.`assigned_to_id`, 
`WorkOrder`.`scheduled_date`, 
`WorkOrder`.`deleted`, 
`WorkOrder`.`quick_books_invoice_number`, 
`WorkOrder`.`is_billable`, 
`WorkOrder`.`maintenance_agreement_id`, 
`WorkOrder`.`maintenance_agreement_work_order_schedule_id`, 
`WorkOrder`.`approved`, 
`WorkOrder`.`schedule_id`, 
`WorkOrder`.`company_id`, 
`WorkOrder`.`department_id`, 
`WorkOrder`.`has_been_reviewed`, 
`WorkOrder`.`work_order_number`,
`WorkOrder`.`review`, 