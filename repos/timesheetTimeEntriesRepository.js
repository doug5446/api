const db = require('../db');

module.exports = {
    async findByID(id) {
        return await db
                    .from('timesheet_time_entries')
                    .where('time_entry_id', id)
    },
    async getAll() {
        return await db
                    .from('timesheet_time_entries')
                    .limit(3)
    }
}
