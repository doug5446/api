const workOrderRepo = require('../repos/workOrderRepository');
const purchaseOrderRepo = require('../repos/purchaseOrderRepository');
const userRepo = require('../repos/userRepository');
const workOrderNotesRepo = require('../repos/workOrderNotesRepository');
const timesheetRepo = require('../repos/timesheetRepository');
const workOrderEmailsRepo = require('../repos/workOrderEmails');
const timeEntriesRepo = require('../repos/timeEntriesRepository');
const timesheetTimeEntriesRepo = require('../repos/timesheetTimeEntriesRepository');

module.exports = {
    async findByID(ctx, next) {
        let id = ctx.params.id;
        let workOrders = await workOrderRepo.findByID(id);
        let purchaseOrders = await purchaseOrderRepo.findByWorkOrderID(id);
        let users = await userRepo.getAll();
        let user = await userRepo.findByID(37);
        let workOrderNotes = await workOrderNotesRepo.getAll();
        let timesheets = await timesheetRepo.getAll();
        let workOrderEmails = await workOrderEmailsRepo.findByIDs([54, 56, 478]);
        let timeEntries = await timeEntriesRepo.findByIDs([54, 56, 478]);
        let timesheetTimeEntries = await timesheetTimeEntriesRepo.getAll()

        ctx.body = {
            timesheetTimeEntries: timesheetTimeEntries, 
            timeEntries: timeEntries,
            workOrderEmails: workOrderEmails,
            timesheets: timesheets,
            workOrderNotes: workOrderNotes,
            user: user,
            workOrders: workOrders,
            purchaseOrders: purchaseOrders,
            users: users
        }
    },      
    async findByCriteria(ctx, next) {
        ctx.body = ctx.query
    }
}
