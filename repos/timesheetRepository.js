const db = require('../db');

module.exports = {
    async findByID(id) {
        return await db
                    .from('timesheets')
                    .where('id', id)
    },
    async getAll() {
        return await db
                    .from('timesheets')
                    .limit(3)
    }
}