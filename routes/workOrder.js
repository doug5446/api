'use strict';
const Router = require('koa-joi-router');
const router = new Router();
const Joi = Router.Joi;
const ctrl = require("../controllers/workOrderController");

const findByIDSchema = {
    id: Joi.number().required()
}

const findByCriteriaSchema = {
    search: Joi.string(),
    status: Joi.number().valid(1,6,5,4,2,8,9,12,11,13,14),
    department: Joi.number(),
    has_agreement: Joi.boolean().truthy('1').falsy('0'),
    deleted: Joi.boolean().truthy('1').falsy('0'),
    type: Joi.array().items(Joi.number().valid(0,1,2,3,4,5,6))
}

router.route({
    method: 'get',
    path: '/workorder/find',
    validate: {
        query: findByCriteriaSchema
    },
    handler: ctrl.findByCriteria
});

router.route({
    method: 'get',
    path: '/workorder/:id',
    validate: {
        params: findByIDSchema
    },
    handler: ctrl.findByID
});

module.exports = router.middleware();