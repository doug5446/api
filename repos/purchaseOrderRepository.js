const db = require('../db');

module.exports = {
    async findByWorkOrderID(workOrderID) {
        return await db
                    .from('purchase_orders')
                    .where('work_order_id', workOrderID)
                    .where('deleted', 0);
    }
}

let columns = [''];


