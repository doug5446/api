const config = require('./config');
var knex = require('knex')({
    client: 'mysql2',
    connection: config.mySqlConfig,
    pool: { min: 0, max: 7 }
})

module.exports = knex