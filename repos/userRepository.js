const db = require('../db');

module.exports = {
    async findByID(id) {
        return await db
                    .from('users')
                    .leftOuterJoin('companies', {'users.company_id': 'companies.id'})
                    .where('users.id', id)
    },
    async getAll() {
        return await db
                    .from('users')
                    //.select(['users.id'])
                    .leftOuterJoin('companies', {'users.company_id': 'companies.id'})
                    .where('users.active', 1)
    }
}