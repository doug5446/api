'use strict';
const request = require('supertest');
const server = require('../app.js');

beforeAll(async () => {
    await server.close()
    console.log('Jest starting!');
});

afterAll(() => {
    server.close();
    console.log('server closed');
});

describe('work order route tests', () => {
    
    describe('find workorder route validation tests', () => {
        test('valid no params', async () => {
            const response = await request(server).get('/workorder/find');
            expect(response.status).toEqual(200);
        });

        test('valid type param', async () => {
            const response = await request(server).get('/workorder/find?type=[0,1,2,3,4,5,6]');
            expect(response.status).toEqual(200);
        });

        test('valid status param', async () => {
            var response;
            response = await request(server).get('/workorder/find?status=1');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?status=2');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?status=13');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?status=14');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?status=12');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?status=11');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?status=6');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?status=4');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?status=8');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?status=9');
            expect(response.status).toEqual(200);
        });

        test('valid type param value', async () => {
            const response = await request(server).get('/workorder/find?type=[0,1,2,3,4,5,6,7]');
            expect(response.status).toEqual(400);
        });

        test('valid has_agreement param', async () => {
            var response;
            response = await request(server).get('/workorder/find?has_agreement=1');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?has_agreement=true');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?has_agreement=false');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?has_agreement=0');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?has_agreement=True');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?has_agreement=False');
            expect(response.status).toEqual(200);
        });

        test('valid deleted param', async () => {
            var response;
            response = await request(server).get('/workorder/find?deleted=1');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?deleted=true');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?deleted=false');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?deleted=0');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?deleted=True');
            expect(response.status).toEqual(200);
            response = await request(server).get('/workorder/find?deleted=False');
            expect(response.status).toEqual(200);
        });

        test('invalid status param', async () => {
            var response;
            response = await request(server).get('/workorder/find?status=a');
            expect(response.status).toEqual(400);
            response = await request(server).get('/workorder/find?status=a');
            expect(response.status).toEqual(400);
            response = await request(server).get('/workorder/find?status=0');
            expect(response.status).toEqual(400);
            response = await request(server).get('/workorder/find?status=15');
            expect(response.status).toEqual(400);
            response = await request(server).get('/workorder/find?status=3');
            expect(response.status).toEqual(400);
        })

        test('invalid department param', async () => {
            const response = await request(server).get('/workorder/find?department=a');
            expect(response.status).toEqual(400);
        });

        test('invalid has_agreement param', async () => {
            const response = await request(server).get('/workorder/find?has_agreement=a');
            expect(response.status).toEqual(400);
        });

        test('invalid deleted param', async () => {
            const response = await request(server).get('/workorder/find?deleted=a');
            expect(response.status).toEqual(400);
        });

        test('invalid type param', async () => {
            const response = await request(server).get('/workorder/find?type=1');
            
            expect(response.status).toEqual(400);

        });


        test('ivalid erroneous params', async () => {
            const response = await request(server).get('/workorder/find?a=1');
            expect(response.status).toEqual(400);
        });
    });

    describe('get single workorder route validation tests', () => {
        test('valid param', async () => {
            const response = await request(server).get('/workorder/1')
            expect(response.status).toEqual(200);
        });
    
        test('invalid no param', async () => {
            const response = await request(server).get('/workorder/')
            expect(response.status).toEqual(404);
        });

        test('invalid letter param', async () => {
            const response = await request(server).get('/workorder/a')
            expect(response.status).toEqual(400);
        });
    
        test('invalid bool param', async () => {
            const response1 = await request(server).get('/workorder/true')
            expect(response1.status).toEqual(400);
        });
    });
    
});
