const db = require('../db');

module.exports = {
    async findByIDs(ids) {
        return await db
                    .from('work_order_emails')
                    .whereIn('work_order_id', ids)
    },
    async getAll() {
        return await db
                    .from('work_order_emails')
                    .limit(3)
    }
}
