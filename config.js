require('dotenv').config()

const mySqlConfig = {
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT
}

module.exports = {
    version: require("pjson").version,
    port: process.env.PORT,
    mySqlConfig: mySqlConfig
}