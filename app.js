'use strict';
const swagger = require('swagger2');
const Koa = require('koa');
const logger = require('koa-logger');
const app = new Koa(); 
const config = require('./config');
//const document  = swagger.loadDocumentSync('./docs/swagger.yml');

// if (!swagger.validateDocument(document)) {
//     throw Error('./swagger.yml does not conform to swagger 2.0 schema')
// }

app.use(logger());

app.use(async (ctx, next) => {
    try {
        await next();
    } catch (err) {
    ctx.status = err.status || 500;
    ctx.body = err.message;
    ctx.app.emit('error', err, ctx);
    }
});

const workOrderRouter = require('./routes/workOrder');
app.use(workOrderRouter);

const server = app.listen(config.port);
module.exports = server;